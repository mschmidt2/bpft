#!/bin/sh
set -x

systemctl stop NetworkManager
rmmod mlx5_ib
rmmod mlx5_core

./dma.bpft &
sleep 2

grep MemAvail /proc/meminfo
modprobe mlx5_core
sleep 2

grep MemAvail /proc/meminfo
ethtool -G enp129s0f0np0 rx 8192 tx 8192
ethtool -G enp129s0f1np1 rx 8192 tx 8192
ip link set dev enp129s0f0np0 up
ip link set dev enp129s0f1np1 up
sleep 2

grep MemAvail /proc/meminfo
kill %1
wait
