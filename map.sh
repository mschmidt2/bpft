#!/usr/bin/sh -xv

PF="enp129s0f0np0"
PCI="0000:81:00.0"

systemctl stop NetworkManager

sleep 2

rmmod mlx5_ib
rmmod mlx5_core

sleep 2

#nohup ./map.bt $PCI ${#PCI} &
./map.bt $PCI ${#PCI} &

sleep 5

modprobe mlx5_core

sleep 5

ethtool -G $PF rx 8192 tx 8192
ip l set dev $PF mtu 9000
ip a add 192.168.42.2/24 dev $PF
ip l set dev $PF up
ethtool -g $PF
ethtool -l $PF

sleep 2

iperf3 -i 5 -t 30 -P 8 --bidir -c 192.168.42.1

ip link set dev $PF down
sleep 5

rmmod mlx5_ib
rmmod mlx5_core
sleep 5

kill %1
wait
